﻿using UnityEditor;
using UnityEngine;

namespace Tools
{
    [CustomEditor(typeof(MonoBehaviour), true, isFallback = true)]
    [CanEditMultipleObjects]
    public class BehaviourButtonsEditor : Editor
    {
        private ButtonAttributeHelper helper = new ButtonAttributeHelper();

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            helper.DrawButtons();
        }

        private void OnEnable()
        {
            helper.Init(target);
        }
    }
}