﻿using System;

namespace Tools
{
    [AttributeUsage(AttributeTargets.Method)]
    public class ButtonAttribute : Attribute { }
}