﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

namespace Complete
{
    public class TankHealth : MonoBehaviour
    {
        public float m_StartingHealth = 100f;               // The amount of health each tank starts with.
        public Slider m_Slider;                             // The slider to represent how much health the tank currently has.
        public Image m_FillImage;                           // The image component of the slider.
		public Image m_BoosterFillImage;                           // The image component of the slider.
		public Text m_healthText;
        public Color m_FullHealthColor = Color.green;       // The color the health bar will be when on full health.
        public Color m_ZeroHealthColor = Color.red;         // The color the health bar will be when on no health.
        public GameObject m_ExplosionPrefab;                // A prefab that will be instantiated in Awake, then used whenever the tank dies.
        public int m_BoosterHealthPerSec = 2;
		public int m_BoosterTime = 6;	
        
        
        private AudioSource m_ExplosionAudio;               // The audio source to play when the tank explodes.
        private ParticleSystem m_ExplosionParticles;        // The particle system the will play when the tank is destroyed.
        private float m_CurrentHealth;                      // How much health the tank currently has.
        private bool m_Dead;                                // Has the tank been reduced beyond zero health yet?

        private float CurrentHealth
        {
			get { return m_CurrentHealth; }
        	set 
        	{ 
        		//check limits

				value = Mathf.Clamp(value, 0, m_StartingHealth);

				m_CurrentHealth = value;

				//Display health value
				m_healthText.text = value.ToString();
				// Set the slider's value appropriately.
				m_Slider.value = value;
	            // Interpolate the color of the bar between the choosen colours based on the current percentage of the starting health.
				m_FillImage.color = Color.Lerp (m_ZeroHealthColor, m_FullHealthColor, value / m_StartingHealth);

				if (value <= 0f && !m_Dead)
	            {
	                OnDeath ();
	            }
        	}
        }


        public void StartHealing()
        {
        	StopAllCoroutines();
			StartCoroutine(Healthing());
        }


		private IEnumerator Healthing()
        {
			for(int i = m_BoosterTime; i > 0; --i)
        	{
				GiveHealth(m_BoosterHealthPerSec);
				m_BoosterFillImage.fillAmount = (float) i / m_BoosterTime;
				yield return new WaitForSecondsRealtime(1);
        	}
			m_BoosterFillImage.fillAmount = 0;
        }


        private void StopHealthing()
        {
        	StopAllCoroutines();
			m_BoosterFillImage.fillAmount = 0f;
        }


        private void Awake ()
        {
            // Instantiate the explosion prefab and get a reference to the particle system on it.
            m_ExplosionParticles = Instantiate (m_ExplosionPrefab).GetComponent<ParticleSystem> ();

            // Get a reference to the audio source on the instantiated prefab.
            m_ExplosionAudio = m_ExplosionParticles.GetComponent<AudioSource> ();

            // Disable the prefab so it can be activated when it's required.
            m_ExplosionParticles.gameObject.SetActive (false);
        }


        private void OnEnable()
        {
            // When the tank is enabled, reset the tank's health and whether or not it's dead.
			CurrentHealth = m_StartingHealth;
            m_Dead = false;
        }


        private void OnDisable()
        {
			StopHealthing();
        }


        public void TakeDamage (float amount)
        {
            // Reduce current health by the amount of damage done.
			CurrentHealth -= amount;
        }


		public void GiveHealth (float amount)
        {
            // Increase current health by the amount of damage done.
			CurrentHealth += amount;
        }

        private void OnDeath ()
        {
            // Set the flag so that this function is only called once.
            m_Dead = true;

            // Move the instantiated explosion prefab to the tank's position and turn it on.
            m_ExplosionParticles.transform.position = transform.position;
            m_ExplosionParticles.gameObject.SetActive (true);

            // Play the particle system of the tank exploding.
            m_ExplosionParticles.Play ();

            // Play the tank explosion sound effect.
            m_ExplosionAudio.Play();

            // Turn the tank off.
            gameObject.SetActive (false);
        }
    }
}