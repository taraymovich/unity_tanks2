﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Complete
{
    public class TankShooting : MonoBehaviour
    {
        public int m_PlayerNumber = 1;             			 // Used to identify the different players.

        [SerializeField] Rigidbody m_Shell;                   // Prefab of the shell.
        [SerializeField] Transform m_FireTransform;           // A child of the tank where the shells are spawned.
        [SerializeField] Slider m_AimSlider;                  // A child of the tank that displays the current launch force.
        [SerializeField] AudioSource m_ShootingAudio;         // Reference to the audio source used to play the shooting audio. NB: different to the movement audio source.
        [SerializeField] AudioClip m_ChargingClip;            // Audio that plays when each shot is charging up.
        [SerializeField] AudioClip m_FireClip;                // Audio that plays when each shot is fired.
        [SerializeField] float m_MinLaunchForce = 15f;        // The force given to the shell if the fire button is not held.
        [SerializeField] float m_MaxLaunchForce = 30f;        // The force given to the shell if the fire button is held for the max charge time.
        [SerializeField] float m_MaxChargeTime = 0.75f;       // How long the shell can charge for before it is fired at max force.
        [SerializeField] int m_BoosterTime = 6;			      // How long one booster is working
		[SerializeField] Image m_FillImage;            		  // The image component of the slider.
		[SerializeField] Text m_MultiplierText;				  // The text component to show current multiplier for damage

        private string m_FireButton;                // The input axis that is used for launching shells.
        private float m_CurrentLaunchForce;         // The force that will be given to the shell when the fire button is released.
        private float m_ChargeSpeed;                // How fast the launch force increases, based on the max charge time.
        private bool m_Fired;                       // Whether or not the shell has been launched with this button press.
        private int m_FireMultiplier = 1; 			// Current damage multiplayer 

        public int FireMultiplier
        {
			get { return m_FireMultiplier; }
			set
			{
				m_FireMultiplier = value;
				m_MultiplierText.text = value + "x";
			}
        }

        public void StartFireBoosting()
        {
        	StopAllCoroutines();
			StartCoroutine(FireBoosting());
        }


        private void StopAllFireBoosting()
        {
        	StopAllCoroutines();
			FireMultiplier = 1;
			m_FillImage.fillAmount = 0;
        }


        private IEnumerator FireBoosting()
        {
			FireMultiplier *= 2;

			float delta = 1f / 10f; //seconds

			for(float time = m_BoosterTime; time >= 0; time -= delta)
        	{
				m_FillImage.fillAmount =  time / m_BoosterTime;
				yield return new WaitForSecondsRealtime(delta);
        	}
			FireMultiplier = 1;
        }


        private void OnEnable()
        {
            // When the tank is turned on, reset the launch force and the UI
            m_CurrentLaunchForce = m_MinLaunchForce;
            m_AimSlider.value = m_MinLaunchForce;
        }


        private void OnDisable()
        {
			StopAllFireBoosting();
        }


        private void Start ()
        {
            // The fire axis is based on the player number.
            m_FireButton = "Fire" + m_PlayerNumber;

            // The rate that the launch force charges up is the range of possible forces by the max charge time.
            m_ChargeSpeed = (m_MaxLaunchForce - m_MinLaunchForce) / m_MaxChargeTime;
        }


        private void Update ()
        {
            // The slider should have a default value of the minimum launch force.
            m_AimSlider.value = m_MinLaunchForce;

            // If the max force has been exceeded and the shell hasn't yet been launched...
            if (m_CurrentLaunchForce >= m_MaxLaunchForce && !m_Fired)
            {
                // ... use the max force and launch the shell.
                m_CurrentLaunchForce = m_MaxLaunchForce;
                Fire ();
            }
            // Otherwise, if the fire button has just started being pressed...
            else if (Input.GetButtonDown (m_FireButton))
            {
                // ... reset the fired flag and reset the launch force.
                m_Fired = false;
                m_CurrentLaunchForce = m_MinLaunchForce;

                // Change the clip to the charging clip and start it playing.
                m_ShootingAudio.clip = m_ChargingClip;
                m_ShootingAudio.Play ();
            }
            // Otherwise, if the fire button is being held and the shell hasn't been launched yet...
            else if (Input.GetButton (m_FireButton) && !m_Fired)
            {
                // Increment the launch force and update the slider.
                m_CurrentLaunchForce += m_ChargeSpeed * Time.deltaTime;

                m_AimSlider.value = m_CurrentLaunchForce;
            }
            // Otherwise, if the fire button is released and the shell hasn't been launched yet...
            else if (Input.GetButtonUp (m_FireButton) && !m_Fired)
            {
                // ... launch the shell.
                Fire ();
            }
        }


        private void Fire ()
        {
            // Set the fired flag so only Fire is only called once.
            m_Fired = true;

            // Create an instance of the shell and store a reference to it's rigidbody.
            Rigidbody shellInstance =
                Instantiate (m_Shell, m_FireTransform.position, m_FireTransform.rotation) as Rigidbody;

			shellInstance.GetComponent<ShellExplosion>().SetDamageMultiplier(m_FireMultiplier);

            // Set the shell's velocity to the launch force in the fire position's forward direction.
            shellInstance.velocity = m_CurrentLaunchForce * m_FireTransform.forward; 

            // Change the clip to the firing clip and play it.
            m_ShootingAudio.clip = m_FireClip;
            m_ShootingAudio.Play ();

            // Reset the launch force.  This is a precaution in case of missing button events.
            m_CurrentLaunchForce = m_MinLaunchForce;
        }
    }
}