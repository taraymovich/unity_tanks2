﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Complete
{
	public class TankPickups : MonoBehaviour 
	{
		public Action<PickupType> Collected;
		public void Pickup(PickupType type)
		{
			if(Collected != null) Collected(type);
		}
	}
}

