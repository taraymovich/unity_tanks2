﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Complete
{
	/*
		This system is creating all pickups
		Steps before Play Mode:
	 	1) Chose BoxCollider - area where pickups will be created 
	 	2) Chose allObstacles - root with all obstacles's colliders
	 	3) Setup others parameters
	 	4) Chose step value and press generate buttons in editor. You can see result in editor view while selecting current gameobject

	 	Steps after PlayMode:
	 		Use public interface to control creating of pickups 

	*/
	public class PickupSystem : MonoBehaviour 
	{
		[Header("Pickup parameters")]
		[SerializeField] List<PickupObject> prefabs = new List<PickupObject>();
		[SerializeField] int m_maxPickupsOnLevel = 5; 			 
		[SerializeField] float m_maxTimeToGenerate = 10f;
		[SerializeField] float m_minTimeToGenerate = 5f;

		private List<PickupObject> pickups = new List<PickupObject>();

		[Header("Grid Generation parameters")]
		[SerializeField] LayerMask m_MaskObstacles;
		[SerializeField] GameObject m_allObstacles;
		[SerializeField] BoxCollider m_box;
		[SerializeField] float m_step;
		[SerializeField] [HideInInspector] List<Vector3> points;

		[Tools.Button]
		public void StartCreatingPickups()
		{
			int startNumber = UnityEngine.Random.Range(0, m_maxPickupsOnLevel + 1);
			for(int i = 0; i < startNumber; ++i) CreateRandomPickup();
			StartCoroutine(PickupCreationLoop());
		}

		[Tools.Button]
		public void StopCreatingPickups()
		{
			StopAllCoroutines();
		}

		[Tools.Button]
		public void ResetAllPickups()
		{
			StopAllCoroutines();
			foreach(var pickup in pickups)
				DestroyImmediate(pickup.gameObject);
			pickups = new List<PickupObject>();
		}


		private IEnumerator PickupCreationLoop()
		{
			while(true)
			{
				yield return new WaitForSecondsRealtime(m_minTimeToGenerate); //wait before next check 

				if(pickups.Count < m_maxPickupsOnLevel)
				{
					float waitTime = UnityEngine.Random.Range(0, m_maxTimeToGenerate - m_minTimeToGenerate);
					yield return new WaitForSecondsRealtime(waitTime);
					CreateRandomPickup();
				}
			}
		}

		/// <summary>
		/// Create one random pickup in one random point
		/// </summary>
		private void CreateRandomPickup()
		{
			int id = UnityEngine.Random.Range(0, prefabs.Count);
			int pointId = UnityEngine.Random.Range(0, points.Count);

			PickupObject newPickup = Instantiate(prefabs[id], points[pointId], Quaternion.identity, transform) as PickupObject; 

			newPickup.Collected += (pickup) => 
			{
				if(pickups.Contains(pickup)) pickups.Remove(pickup);
			};
			pickups.Add(newPickup);
		}

		[Tools.Button]
		/// <summary>
		/// Generate all positions for current level
		/// </summary>
		private void GeneratePositions()
		{
			points = new List<Vector3>();
			Collider[] obstacles = m_allObstacles.GetComponentsInChildren<Collider>();

			for(float x = m_box.bounds.min.x; x <= m_box.bounds.max.x; x+= m_step)
				for(float z = m_box.bounds.min.z; z <= m_box.bounds.max.z; z+= m_step){
					Vector3 position = new Vector3(x, m_step / 2f, z);
					if(!Intersects(position, ref obstacles)) {
						position.y = 0;
						points.Add(position);
					}
				}
		}

		/// <summary>
		/// Visualize all generated points
		/// </summary>
		private void OnDrawGizmosSelected()
		{
			Vector3 size = Vector3.one * m_step / 2f;
			Vector3 shift = Vector3.up * m_step / 2f;

			for(int i = 0; i < points.Count; ++i)
			{
				Gizmos.DrawCube(points[i] + shift, size);
			}
			
		}

		/// <summary>
		/// Check if point (cube) intersect any of colliders 
		/// </summary>
		private bool Intersects(Vector3 position, ref Collider[] obstacles)
		{
			Bounds bounds = new Bounds(position, Vector3.one * m_step);

			foreach(var collider in obstacles){
				if((m_MaskObstacles == (m_MaskObstacles | (1 << collider.gameObject.layer))) 
				&& 
				collider.bounds.Intersects(bounds)) return true;
			}

			return false;
		}
	}
}
