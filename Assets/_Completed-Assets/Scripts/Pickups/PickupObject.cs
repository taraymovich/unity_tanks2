﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Complete
{
	public class PickupObject : MonoBehaviour 
	{
		[SerializeField] LayerMask m_TankMask; 
		[SerializeField] PickupType m_Type;
		[SerializeField] ParticleSystem m_Particles;         // Reference to the particles that will play on collection.

		public Action<PickupObject> Collected;

		protected virtual void OnTriggerEnter (Collider other)
        {
			if(m_TankMask != (m_TankMask | (1 << other.gameObject.layer))) return;

			TankPickups tank = other.gameObject.GetComponent<TankPickups>();

			tank.Pickup(m_Type);

			ParticleSystem particles = ObjectPool.Instantiate(m_Particles, transform.position, transform.rotation);

            // Play the particle system.
			particles.Play();

            // Play the explosion sound effect.
			particles.GetComponent<AudioSource>().Play();

			ObjectPool.Destroy(particles);

			if(Collected != null) Collected(this);

            // Destroy the shell.
            Destroy (gameObject);
        }
	}
}
