﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Complete
{
	//Helper to keep forward rotation for UI elements
	public static class CameraOrientation 
	{
		private static Quaternion forwardRotation;
		public static Quaternion ForwardRotation { get {return forwardRotation; } }

		private static Camera camera;

		public static float OrthoSize { get {return camera? camera.orthographicSize : 10; } }

		public static void Init(Camera _camera)
		{
			camera = _camera;
			forwardRotation = Quaternion.LookRotation(camera.transform.forward, Vector3.up);
		}
	}
}
