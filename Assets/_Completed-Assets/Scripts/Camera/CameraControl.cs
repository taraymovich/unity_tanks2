﻿using UnityEngine;

namespace Complete
{
	/* 
	Current camera contoller
	1) will include all targets in camera view 
	2) Exclude level boarders
	3) in case targets are located in maximum opposite positions of the level (example : the top right and the bottom left)
		and level sizes is not mathhing screen sizes - 
		boardes have to be visible to keep targets also visible 
	*/
    public class CameraControl : MonoBehaviour
    {
    	[SerializeField] BoxCollider bounds;				// Level bounds
        [SerializeField] float m_DampTime = 0.2f;          	// Approximate time for the camera to refocus.
        [SerializeField] float m_ScreenEdgeBuffer = 4f; 	// Space between the top/bottom most target and the screen edge.
        [HideInInspector] public Transform[] m_Targets; 	// All the targets the camera needs to encompass.
       
        private Camera m_Camera;                        	// Used for referencing the camera.
        private float m_ZoomSpeed;                      	// Reference speed for the smooth damping of the orthographic size.
        private Vector3 m_MoveVelocity;                 	// Reference velocity for the smooth damping of the position.
		private Vector3 m_cameraNormalizedProjection;		// Normalized camera projection into plane
		private float m_projectionAspect;
		private Bounds targetBounds;

		private float prevScreenHeight, prevScreenWidth;	// To detect window resize event

        private void Awake ()
        {
            m_Camera = GetComponentInChildren<Camera> ();

			m_cameraNormalizedProjection = GetCameraProjection();
			m_projectionAspect = m_cameraNormalizedProjection.z / m_cameraNormalizedProjection.x;

			CameraOrientation.Init(m_Camera);
        }


        private void Update()
        {
        	//detect if window was resized
	        if(prevScreenWidth != Screen.width || prevScreenHeight != Screen.height)
	        {
	        	prevScreenWidth = Screen.width;
	        	prevScreenHeight = Screen.height;

	        	//recalculate values 
				m_cameraNormalizedProjection = GetCameraProjection();
				m_projectionAspect = m_cameraNormalizedProjection.z / m_cameraNormalizedProjection.x;
	        }
        }
       
        private void FixedUpdate ()
		{
            // Get target camera bounds
			targetBounds = GetCameraBounds();

        	//apply position
			transform.position = Vector3.SmoothDamp(transform.position, targetBounds.center, ref m_MoveVelocity, m_DampTime);

			//apply zoom
			m_Camera.orthographicSize = Mathf.SmoothDamp (m_Camera.orthographicSize, targetBounds.extents.z / m_cameraNormalizedProjection.z, ref m_ZoomSpeed, m_DampTime);
        }

        /// <summary>
        /// Draw current bounds in editor view
        /// </summary>
        private void OnDrawGizmosSelected()
        {
			Gizmos.DrawCube(targetBounds.center, targetBounds.size);
        }

        /// <summary>
        /// Get current camera bounds :
        /// 1) Contains all targets
        /// 2) Has same aspect as camera
        /// 3) Don't include level borders (until it is possible due to level aspect ratio)
        /// </summary>
		private Bounds GetCameraBounds()
        {
        	//Include all points in the bounds
			Bounds cameraBounds = new Bounds(m_Targets[0].position, Vector3.zero);

        	for(int i = 1; i < m_Targets.Length; ++i)
        	{
				cameraBounds.Encapsulate(m_Targets[i].position);
        	}

        	//Add space on each side
			cameraBounds.Expand(Vector3.one *  m_ScreenEdgeBuffer);

			//Limit bound by level
			cameraBounds.SetMinMax(
				Vector3.Max(cameraBounds.min, bounds.bounds.min), 
				Vector3.Min(cameraBounds.max, bounds.bounds.max));

			//Keep same aspect as camera
			Vector3 size = cameraBounds.size;
			if(cameraBounds.size.z / cameraBounds.size.x > m_projectionAspect) //need to increase x size 
			{
				size.x = cameraBounds.size.z / m_projectionAspect;
			} 
			else  //need to increase z size 
			{
				size.z = cameraBounds.size.x * m_projectionAspect;
			}
			cameraBounds.size = size;

			//Move center to keep camera within bounds
			Vector3 center = cameraBounds.center;

			size = bounds.bounds.extents - cameraBounds.extents;

			if(size.x >= 0) center.x = Mathf.Clamp(center.x, bounds.center.x - size.x, bounds.center.x + size.x); else center.x = bounds.bounds.center.x;
			if(size.z >= 0) center.z = Mathf.Clamp(center.z, bounds.center.z - size.z, bounds.center.z + size.z); else center.z = bounds.bounds.center.z;

			cameraBounds.center = center;

			return cameraBounds;
        }

        /// <summary>
        /// Get camera projection size into level plane
        /// </summary>
        private Vector3 GetCameraProjection()
        {
			//level plane
        	Plane plane = new Plane(Vector3.up, Vector3.zero);
        
        	//real camera up and right direction
			Vector3 cameraUp = m_Camera.transform.up;
			Vector3 cameraRight = m_Camera.transform.right * m_Camera.aspect;

			//rays of two points of camera bounds
			Ray ray1 = new Ray(cameraUp + cameraRight, m_Camera.transform.forward);
			Ray ray2 = new Ray(cameraUp - cameraRight, m_Camera.transform.forward);

			//find intersections of rays with plane
			float enter1 = 0, enter2 = 0;

			plane.Raycast(ray1, out enter1);
			plane.Raycast(ray2, out enter2);

			Vector3 v1 = ray1.GetPoint(enter1);
			Vector3 v2 = ray2.GetPoint(enter2);

		
			return new Vector3(
				Mathf.Max(Mathf.Abs(v1.x), Mathf.Abs(v2.x)),  //for right direction
				0,
				Mathf.Max(Mathf.Abs(v1.z), Mathf.Abs(v2.z))); //for forward direction
        }

        public void SetStartPositionAndSize ()
        {
			// Get target camera bounds
			targetBounds = GetCameraBounds();
			// Apply position
			transform.position = bounds.center;
			// Apply zoom
			m_Camera.orthographicSize =  targetBounds.extents.z / m_cameraNormalizedProjection.z;
        }
    }
}