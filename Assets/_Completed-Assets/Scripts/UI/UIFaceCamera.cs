﻿using UnityEngine;

namespace Complete
{
	// This class is used to make sure world space UI
    // elements face the camera direction.
    public class UIFaceCamera : MonoBehaviour
    {
	    private void Update ()
	    {
	    	transform.rotation = CameraOrientation.ForwardRotation;
			transform.localScale = Vector3.one * CameraOrientation.OrthoSize / 10;
	    }
    }
}