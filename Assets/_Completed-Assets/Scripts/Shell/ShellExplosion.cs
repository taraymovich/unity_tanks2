using UnityEngine;

namespace Complete
{
    public class ShellExplosion : MonoBehaviour
    {
        [SerializeField] LayerMask m_TankMask;                        // Used to filter what the explosion affects, this should be set to "Players".
        [SerializeField] ParticleSystem m_ExplosionParticles;         // Reference to the particles that will play on explosion.
        [SerializeField] AudioSource m_ExplosionAudio;                // Reference to the audio that will play on explosion.
        [SerializeField] float m_MaxDamage = 10f;                    // The amount of damage done if the explosion is centred on a tank.
        [SerializeField] float m_ExplosionForce = 1000f;              // The amount of force added to a tank at the centre of the explosion.
        [SerializeField] float m_MaxLifeTime = 2f;                    // The time in seconds before the shell is removed.
        [SerializeField] float m_ExplosionRadius = 5f;                // The maximum distance away from the explosion tanks can be and are still affected.

        private bool isTriggered = false;

		public void SetDamageMultiplier(float damageMultiplier)
		{
			m_MaxDamage *= damageMultiplier;
		}

        private void Start ()
        {
            // If it isn't destroyed by then, destroy the shell after it's lifetime.
            Destroy (gameObject, m_MaxLifeTime);
        }
       
        private void OnTriggerEnter (Collider other)
        {
        	if(isTriggered) return;

        	isTriggered = true;
			// Collect all the colliders in a sphere from the shell's current position to a radius of the explosion radius.
            Collider[] colliders = Physics.OverlapSphere (transform.position, m_ExplosionRadius, m_TankMask);

            // Go through all the colliders...
            for (int i = 0; i < colliders.Length; i++)
            {
                // ... and find their rigidbody.
                Rigidbody targetRigidbody = colliders[i].GetComponent<Rigidbody> ();

                // If they don't have a rigidbody, go on to the next collider.
                if (!targetRigidbody)
                    continue;

                // Add an explosion force.
                targetRigidbody.AddExplosionForce (m_ExplosionForce, transform.position, m_ExplosionRadius);

                // Find the TankHealth script associated with the rigidbody.
                TankHealth targetHealth = targetRigidbody.GetComponent<TankHealth> ();

                // If there is no TankHealth script attached to the gameobject, go on to the next collider.
                if (!targetHealth)
                    continue;

                // Calculate the amount of damage the target should take based on it's distance from the shell.
               	// float damage = CalculateDamage (targetRigidbody.position);
               	// Always same damage
				float damage = m_MaxDamage;

                // Deal this damage to the tank.
                targetHealth.TakeDamage (damage);
            }

			ParticleSystem particles = ObjectPool.Instantiate(m_ExplosionParticles, transform.position, Quaternion.identity);
			particles.Play();
			particles.GetComponent<AudioSource>().Play();

			ObjectPool.Destroy (particles);

            // Destroy the shell.
            Destroy (gameObject);
        }
    }
}