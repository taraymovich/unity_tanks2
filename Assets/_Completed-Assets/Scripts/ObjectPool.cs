﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.SceneManagement;
 

namespace Complete
{
	public static class ObjectPool
	{
		static ObjectPool()
		{
			GameObject obj = new GameObject("ObjectPool Root");
			mono = obj.AddComponent<MonoEmpty>();
			root = obj.transform;
			MonoBehaviour.DontDestroyOnLoad(obj);

			SceneManager.sceneLoaded += OnSceneLoaded;
		}

		static void OnSceneLoaded (Scene arg0, LoadSceneMode arg1)
		{
			// Put all non destroyes instances back 
			foreach(var pair in instances)
			{
				objects[pair.Value].Add(pair.Key);

				pair.Key.transform.SetParent(root);
				pair.Key.gameObject.SetActive(false);
			}

			instances = new Dictionary<Component, Component>();
		} 

		private static MonoEmpty mono;
		private static Transform root;

		private static Dictionary<Component, List<Component>> objects = new Dictionary<Component, List<Component>>();
		private static Dictionary<Component, Component> instances = new Dictionary<Component, Component>();

		public static T Instantiate<T>(T prefab) where T : Component
		{
			if(!objects.ContainsKey(prefab)) objects.Add(prefab, new List<Component>());

			T instance;

			if(objects[prefab].Count == 0)
			{
				instance =  Component.Instantiate(prefab);
			} 
			else 
			{
				instance = objects[prefab][0] as T;
				objects[prefab].RemoveAt(0);
			}

			instance.gameObject.SetActive(true);
			instance.transform.SetParent(null);
			instances.Add(instance, prefab);

			return instance;

		}

		public static T Instantiate<T>(T prefab, Vector3 position, Quaternion rotation) where T : Component
		{
			if(!objects.ContainsKey(prefab)) objects.Add(prefab, new List<Component>());

			T instance;

			if(objects[prefab].Count == 0)
			{
				instance =  Component.Instantiate(prefab, position, rotation);
			} 
			else 
			{
				instance = objects[prefab][0] as T;
				objects[prefab].RemoveAt(0);
				instance.transform.SetPositionAndRotation(position, rotation);
			}

			instance.gameObject.SetActive(true);
			instance.transform.SetParent(null);
			instances.Add(instance, prefab);

			return instance;

		}

		public static void Destroy<T>(T instance) where T : Component
		{
			if(instances.ContainsKey(instance))
			{
				Component prefab = instances[instance];

				objects[prefab].Add(instance);
				instances.Remove(instance);

				instance.transform.SetParent(root);
				instance.gameObject.SetActive(false);
			} 
			else 
			{
			 	//current object not found
			 	Component.DestroyImmediate(instance);
			}
		}

		public static void Destroy<T>(T instance, float time) where T : Component
		{
			mono.StartCoroutine(DestroyDelay(instance, time));
		}

		public static void Destroy(ParticleSystem instance) //special destroy for particles systems
		{
			ParticleSystem.MainModule mainModule = instance.main;
			mono.StartCoroutine(DestroyDelay(instance, mainModule.duration));
		}

		private static IEnumerator DestroyDelay<T>(T instance, float time) where T : Component
		{
			yield return new WaitForSeconds(time);
			Destroy(instance);
		}
	}
}
